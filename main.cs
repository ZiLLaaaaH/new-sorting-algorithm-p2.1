private void QuickSort(long[] inputArray)
{
    int left = 0;
    int right = inputArray.Length - 1;
    InternalQuickSort(inputArray, left, right);
}

<summary>
// Internal recursive sort algorithm for quick sort
// using divide and conquer. Sorting is done based on pivot
</summary>
<param name="inputArray"></param>
<param name="left"></param>
<param name="right"></param>
private void InternalQuickSort(long[] inputArray, int left, int right)
{
    int pivotNewIndex = Partition(inputArray, left, right);
    long pivot = inputArray[(left + right) / 2];
    if (left < pivotNewIndex-1)
        InternalQuickSort(inputArray, left, pivotNewIndex - 1);
    if (pivotNewIndex < right)
        InternalQuickSort(inputArray, pivotNewIndex, right);
}

//This operation returns a new pivot everytime it is called recursively
//and swaps the array elements based on pivot value comparison
private int Partition(long[] inputArray, int left, int right)
{
    int i = left, j = right;
    long pivot = inputArray[(left + right) / 2];

    while (i <= j)
    {
        while (inputArray[i] < pivot)
            i++;
        while (inputArray[j] < pivot)
            j--;
        if (i <= j)
        {
            SwapWithTemp(ref inputArray[i], ref inputArray[j]);
            i++; j--;
        }
    }
    return i;
}